[Service]
WorkingDirectory=/home/pi/
ExecStart=node /home/pi/arducopter-hexa -C /dev/ttyAMA0
Restart=always
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=arducopter-hexa
User=root
Group=root
Environment=NODE_ENV=production
[Install]
WantedBy=multi-user.target